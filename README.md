# Quick Weight Loss Center Interview Technical Question
_____________________________________________________________

##Setup Instructions
To setup this project, please run "npm install" or "yarn install" in the directory created by cloning this repo.

OR

Download the archive of this repo and extract it to a desired location on your workstation. 

From there you should be able to run this landing page locally without a webserver by using navigating to the "index.html" and viewing it with you preferred web browser.

Thank you.

##Question 1:
Tell us about an important technical decision you had to make on a recent project. 

###What was the situation?

In house developer takes 6-8 house to rebuild application server (LAMP Stack - Redhat 7.2, Apache 2.2, MySql 5.7, PHP 7.0) and deploy application with database.

###How did you understand the problem?

I understood that all the instructions in the disaster recover manual could be turned into a script which could execute to completion in 5 to 30 mins.  The in-house resource was and engineer and was overloaded; and did not have time to learn new methodologies. 

###What were your options? 

I could continue to use the existing manual to recreate the development and new application server on a virtual server (company IT Standards), or I could use a physical server.|
I could take bundle all the commands used setup and configure the application server (LAMP Stack)

###What informed your final decision? 

Company Policies, Stack Holders, IT Best Practices, Software development best practices

###Who else was involved? 

One In-House Resource, his director, IT personnel for the 2 companies that would be using the software.-

###What was the outcome? 

I was able to complete a script that would take setup the software environment for the web application on a newly deployed Virtual Machine with Redhat 7.2 in anywhere from 5 to 20 mins, down from 6-8 hours.

###You may create diagrams but please write in narrative form: we want an essay, not a presentation.

I was contracted to update a propriety application in the summer of 2017 which was created in the early 2000's and was over due for updates and added features.  The application was running on a physical box configured with Ubuntu 8, Apache 1, MySql 5.1, and PHP 5.2., and not managed or accessible by company IT resources.  This was outside the company standards and best practices.  

This application was developed by 2 in-house resources, one of which was now retired, the second being a mechanical / electronic engineer, who had limited bandwidth to keep up with the software development world.  The engineer, being the only resource managing this application, insisted that he have full control of the system.  He took exception to working with the companies I.T. resources, who typically would take weeks to resolve users issues. 

The wedge that existed here was that the engineer wanted to stay away from the I.T. department and wanted to keep things as close to his existing setup as possible. 

I traveled to the site location and met with the project stakeholders, and various people resources I would be interacting with to complete the project.  After spending some time with the engineer who co-authored the application, I was given a lengthy disaster recovery manual.  This manual detailed a 6 - 8 hour process of rebuilding the server environment and setup of the application server from scratch.

I completed the acquisition of the entire application and returned home. The first order of business was to create the development and production server environments, which should take a long time, but it didn't, as I use containerization (Vagrant / Docker) to build and test my applications.  These tools allow you to save time by using scripts to run the various installation and configuration tasks to get the environment setup; this is known as provisioning. This also helps me keep the various server environments identical, and removes much of the bugs that show up in software development when applications are running on environments that differ.

This also went hand in hand with using the company's Virtual Server infrastructure, which would allow the engineer to spin up a new server in 5 minutes, and had a 35 day backup retention. To support this move, we trained our engineer on managing his virtual server through a web interface, after which he could login and run the script I created to bring the process of spinning up the application from zero to fully functional in under 30 minutes.

In conclusion, we were able to get the in-house resource trained so that he would feel comfortable with the configuration and allow him to manage the server without having to wait on I.T. I relieved the anxiety that he had of having to learn a new OS, setup all the tools he had been using in his setup to minimize changes to his work-flow, and finally, and I removed the need for the manual back up ritual he performed on a weekly basis. 

Thank you for the manual, no thank you for the 6 - 8 hours.



##Question 2

###Using CSS, HTML, and JavaScript, create a landing page to promote a bundled packaged good product that is sold as a subscription (think Dollar Shave Club).

###My write-up:
Caribbean Kitchen Prepared Meals!!!

I am from Jamaica, and I love my island food, and that food doesn't love me;  not all of it.  I thought of this since I imagine that I am not the only one that feels this way.  Selecting the leaner, healthier menu items to create prepared meals for lovers of island food would be a welcomed entry into the food subscription space.

Simple choose from the available menu options, We cook & Deliver, and you Enjoy.

I choose Bootstrap and JQuery to create this landing page as Landing pages are fairly simple and don't need to do any heavy lifting. 

I use a variety of development tools which include NetBeans.  To reduce the time I started with a Initializr: Bootstrap boilerplate (HTML5, Bootstrap 3.3.1, JQuery 1.11.2), which the NetBeans IDE provides.

From there I ran "yarn install" in the root of the application folder to pull in all the required dependencies.
I use yarn, which is Facebooks rework of npm (Node Package Manager), which also uses package.json.

From there I created a rough draft of what I wanted to see on paper, and then went looking for images and icons.

I took one of the images I found that I could break apart and add a white background for the banner / jumbotron and then garnished 
it with a simple form.  Every good landing page needs a simple form to capture leads into the new customer acquisition funnel. 
If the visitor got to the Landing page then they we know they we interested. These simple forms get more people to click. From there we can let the call center do the closing.


If I had more time I do more work on branding, product options and presentation, create variations of the landing page, and setup live demos.




